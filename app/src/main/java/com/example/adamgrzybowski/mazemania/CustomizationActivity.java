package com.example.adamgrzybowski.mazemania;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import game.level.Cell;
import game.level.LevelCreator;
import game.modes.GameMode;

public class CustomizationActivity extends Activity {
    int mode = 0;
    int points = 5;
    int enemies = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mode = getIntent().getIntExtra("MODE", 0);

        setContentView(R.layout.customization);

        switch (mode) {
            case GameMode.classicMazeMode:
                ((TextView)findViewById(R.id.mode_name)).setText(R.string.classic_mode);
                ((TextView)findViewById(R.id.mode_description)).setText(R.string.classic_mode_description);
                findViewById(R.id.map_size_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.map_size_radio).setVisibility(View.VISIBLE);
                break;
            case GameMode.diamondsMode:
                ((TextView)findViewById(R.id.mode_name)).setText(R.string.diamonds_mode);
                ((TextView)findViewById(R.id.mode_description)).setText(R.string.diamonds_mode_description);
                findViewById(R.id.map_size_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.map_size_radio).setVisibility(View.VISIBLE);
                findViewById(R.id.number_points_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.number_points_radio).setVisibility(View.VISIBLE);
                break;
            case GameMode.escapeMode:
                ((TextView)findViewById(R.id.mode_name)).setText(R.string.escape_mode);
                ((TextView)findViewById(R.id.mode_description)).setText(R.string.escape_mode_description);
                findViewById(R.id.map_size_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.map_size_radio).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_radio).setVisibility(View.VISIBLE);
                break;
            case GameMode.raceMode:
                ((TextView)findViewById(R.id.mode_name)).setText(R.string.race_mode);
                ((TextView)findViewById(R.id.mode_description)).setText(R.string.race_mode_description);
                findViewById(R.id.map_size_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.map_size_radio).setVisibility(View.VISIBLE);
                findViewById(R.id.number_points_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.number_points_radio).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_radio).setVisibility(View.VISIBLE);
                break;
            case GameMode.territoryMode:
                ((TextView)findViewById(R.id.mode_name)).setText(R.string.territory_mode);
                ((TextView)findViewById(R.id.mode_description)).setText(R.string.territory_mode_description);
                findViewById(R.id.map_size_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.map_size_radio).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_tv).setVisibility(View.VISIBLE);
                findViewById(R.id.number_enemies_radio).setVisibility(View.VISIBLE);
                break;
        }
    }

    public void smallMapSizeButton(View view) {
        LevelCreator.levelWidth = 8;
        LevelCreator.levelHeight = 14;
    }

    public void mediumMapSizeButton(View view) {
        LevelCreator.levelWidth = 12;
        LevelCreator.levelHeight = 20;
    }

    public void bigMapSizeButton(View view) {
        LevelCreator.levelWidth = 16;
        LevelCreator.levelHeight = 30;
    }

    public void smallNumberPoints(View view) {
        points = 5;
    }

    public void mediumNumberPoints(View view) {
        points = 10;
    }

    public void bigNumberPoints(View view) {
        points = 25;
    }

    public void smallNumberEnemies(View view) {
        enemies = 3;
    }

    public void mediumNumberEnemies(View view) {
        enemies = 7;
    }

    public void bigNumberEnemies(View view) {
        enemies = 15;
    }


    public void startGameButton(View view) {
        Cell.cellWidth = Resources.getSystem().getDisplayMetrics().widthPixels / LevelCreator.levelWidth;
        Cell.cellHeight = Resources.getSystem().getDisplayMetrics().heightPixels / LevelCreator.levelHeight;

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("ENEMIES", enemies);
        intent.putExtra("POINTS", points);
        intent.putExtra("MODE", mode);
        startActivity(intent);
    }
}
