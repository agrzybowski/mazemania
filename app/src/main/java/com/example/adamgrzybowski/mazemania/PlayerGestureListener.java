package com.example.adamgrzybowski.mazemania;

import android.view.GestureDetector;
import android.view.MotionEvent;

import game.others.Controls;
import game.level.Player;

public class PlayerGestureListener implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private Controls controls;
    private Player player;

    PlayerGestureListener(Controls controls, Player player) {
        this.controls = controls;
        this.player = player;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float diffY = e2.getY() - e1.getY();
        float diffX = e2.getX() - e1.getX();

        if(Math.abs(diffX) > Math.abs(diffY)) {
            if(diffX > 0) {
                if(controls.isPossible(0, 1)) {
                    player.setDirectionY(0);
                    player.setDirectionX(1);
                }
                return true;
            } else {
                if(controls.isPossible(0, -1)) {
                    player.setDirectionY(0);
                    player.setDirectionX(-1);
                }
                return true;
            }
        } else {
            if(diffY > 0) {
                if(controls.isPossible(1, 0)) {
                    player.setDirectionY(1);
                    player.setDirectionX(0);
                }
                return true;
            } else {
                if(controls.isPossible(-1, 0)) {
                    player.setDirectionY(-1);
                    player.setDirectionX(0);
                }
                return true;
            }
        }
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        player.setDirectionY(0);
        player.setDirectionX(0);
        return false;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }


    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}
