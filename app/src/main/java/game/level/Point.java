package game.level;

public class Point extends Cell {
    private boolean collected;

    public Point(Cell cell) {
        super(cell.getY(), cell.getX());
        setCollected(false);
        setWallDown(cell.isWallDown());
        setWallLeft(cell.isWallLeft());
        setWallRight(cell.isWallRight());
        setWallUp(cell.isWallUp());
    }

    public boolean isCollected() {
        return collected;
    }

    public void setCollected(boolean collected) {
        this.collected = collected;
    }
}
