package game.level;

import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.graphics.Bitmap;

public class Cell {
    public static int cellHeight;
    public static int cellWidth;

    private int y;
    private int x;
    private int drawY;
    private int drawX;
    private boolean wallUp;
    private boolean wallRight;
    private boolean wallLeft;
    private boolean wallDown;
    private Player visitedBy;

    public Cell(int y, int x) {
        this.y = y;
        this.x = x;
        this.drawY = y * Cell.cellHeight;
        this.drawX = x * Cell.cellWidth;

        this.wallUp = true;
        this.wallRight = true;
        this.wallDown = true;
        this.wallLeft = true;
        this.visitedBy = null;
    }

    public boolean isWallDown() {
        return wallDown;
    }

    public boolean isWallLeft() {
        return wallLeft;
    }

    public boolean isWallRight() {
        return wallRight;
    }

    public boolean isWallUp() {
        return wallUp;
    }

    public void breakWallTo(Cell cell) {
        int deltaX = getX() - cell.getX();
        int deltaY = getY() - cell.getY();
        switch (deltaX) {
            case 0:
                switch (deltaY) {
                    case -1:
                        //DOWN
                        wallDown = false;
                        cell.setWallUp(false);
                        break;
                    case 1:
                        //UP
                        wallUp = false;
                        cell.setWallDown(false);
                        break;
                }
                break;
            case 1:
                //LEFT
                wallLeft = false;
                cell.setWallRight(false);
                break;
            case -1:
                //RIGHT
                wallRight = false;
                cell.setWallLeft(false);
                break;
        }
    }

    public boolean canEnterFrom(int y, int x) {
        int deltaX = getX() - x;
        int deltaY = getY() - y;
        switch (deltaX) {
            case 0:
                switch (deltaY) {
                    case -1:
                        //DOWN
                        return !isWallDown();
                    case 1:
                        //UP
                        return !isWallUp();
                }
                break;
            case 1:
                //LEFT
                return !isWallLeft();
            case -1:
                return !isWallRight();
        }
        return false;
    }

    public Player getVisitedBy() {
        return visitedBy;
    }

    public boolean isVisited() {
        return visitedBy != null;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getDrawY() {
        return drawY;
    }

    public int getDrawX() {
        return drawX;
    }

    public void setDrawX(int drawX) {
        this.drawX = drawX;
    }

    public void setDrawY(int drawY) {
        this.drawY = drawY;
    }

    public void setWallDown(boolean wallDown) {
        this.wallDown = wallDown;
    }

    public void setWallLeft(boolean wallLeft) {
        this.wallLeft = wallLeft;
    }

    public void setWallRight(boolean wallRight) {
        this.wallRight = wallRight;
    }

    public void setWallUp(boolean wallUp) {
        this.wallUp = wallUp;
    }

    public void setVisitedBy(Player visitedBy) {
        this.visitedBy = visitedBy;
    }
}
