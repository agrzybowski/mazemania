package game.others;

import game.level.Cell;
import game.modes.GameMode;
import game.level.Player;
import game.level.Point;
import game.level.LevelCreator;

public class Controls {
    private final Cell[][] map;
    private Player player;
    private GameMode gameMode;

    public Controls(GameMode gameMode, Player player) {
        this.map = gameMode.getMap();
        this.player = player;
        this.gameMode = gameMode;
    }

    public int countNerbyRoutes(int y, int x) {
        int nerbyRoutes = 0;

        if (y + 1 < LevelCreator.levelHeight && !map[y + 1][x].isWallUp()) {
            nerbyRoutes++;
        }
        if (y - 1 >= 0 && !map[y - 1][x].isWallDown()) {
            nerbyRoutes++;
        }
        if (x + 1 < LevelCreator.levelWidth && !map[y][x + 1].isWallLeft()) {
            nerbyRoutes++;
        }
        if (x - 1 >= 0 && !map[y][x - 1].isWallRight()) {
            nerbyRoutes++;
        }

        return nerbyRoutes;
    }

    public boolean isPossible(int deltaY, int deltaX) {
        return checkIsNotOutOfMap(deltaY, deltaX) && map[player.getY() + deltaY][player.getX() + deltaX].canEnterFrom(player.getY(), player.getX());
    }

    private boolean checkIsNotOutOfMap(int deltaY, int deltaX) {
        return player.getY() + deltaY >= 0 &&
                player.getY() + deltaY < LevelCreator.levelHeight &&
                player.getX() + deltaX >= 0 &&
                player.getX() + deltaX < LevelCreator.levelWidth;
    }


    private boolean event() {
        if (map[player.getY()][player.getX()].getClass().equals(Point.class) && !((Point) map[player.getY()][player.getX()]).isCollected()) {
            ((Point) map[player.getY()][player.getX()]).setCollected(true);
            player.addPointsCollected();
            return false;
        }

        if (countNerbyRoutes(player.getY(), player.getX()) >= 3) {
            return true;
        }

        return false;
    }

    public void move() {
        int deltaY = player.getDirectionY();
        int deltaX = player.getDirectionX();
        if (deltaY != 0 || deltaX != 0) {
            if (!isPossible(deltaY, deltaX)) {
                if(!tryToChangeDirection(deltaY, deltaX)) {
                    return;
                }
            }

            deltaY = player.getDirectionY();
            deltaX = player.getDirectionX();

            if (map[player.getY()][player.getX()].getVisitedBy() != null) {
                map[player.getY()][player.getX()].getVisitedBy().subCellsVisited();
            }
            player.addCellsVisited();
            gameMode.putTrail(player, map[player.getY()][player.getX()], map[player.getY() + deltaY][player.getX() + deltaX]);
            player.setY(player.getY() + deltaY);
            player.setX(player.getX() + deltaX);
            player.addDistance();

            if (event()) {
                player.setDirectionY(0);
                player.setDirectionX(0);
            }
        }
    }

    public boolean tryToChangeDirection(int deltaY, int deltaX) {
        if (checkIsNotOutOfMap(deltaX, deltaY) &&
                map[player.getY() + deltaX][player.getX() + deltaY].canEnterFrom(player.getY(), player.getX())) {
            player.setDirectionX(deltaY);
            player.setDirectionY(deltaX);
        } else {
            if (checkIsNotOutOfMap(-deltaX, -deltaY) &&
                    map[player.getY() - deltaX][player.getX() - deltaY].canEnterFrom(player.getY(), player.getX())) {
                player.setDirectionX(-deltaY);
                player.setDirectionY(-deltaX);
            } else {
                player.setDirectionX(0);
                player.setDirectionY(0);
                return false;
            }
        }
        return true;
    }

    public boolean checkNextIsVisited(int deltaY, int deltaX) {
        return checkIsNotOutOfMap(deltaY, deltaX) &&
                map[player.getY() + deltaY][player.getX() + deltaX].isVisited() &&
                map[player.getY() + deltaY][player.getX() + deltaX].getVisitedBy().equals(player);
    }
}
