package game.others;

import android.app.Activity;
import android.content.Context;

import com.example.adamgrzybowski.mazemania.GameActivity;

import game.others.Controls;
import game.bot.BotThread;
import game.modes.GameMode;

public class Supervisor extends Thread {
    private Controls[] opponentsControls;
    private Controls playerControls;
    private GameMode gameMode;
    private BotThread[] botThreads;
    private Context context;

    public Supervisor(Context context, GameMode gameMode, BotThread[] botThreads, Controls[] opponentsControls, Controls playerControls) {
        this.playerControls = playerControls;
        this.opponentsControls = opponentsControls;
        this.gameMode = gameMode;
        this.botThreads = botThreads;
        this.context = context;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(300);
                for (BotThread botThread: botThreads) {
                    new Thread(botThread).start();
                }

                for (Controls opponentControls : opponentsControls) {
                    opponentControls.move();
                    if(check()) {
                        return;
                    }
                }

                playerControls.move();
                if(check()) {
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean check() {
        switch (gameMode.checkGameStatus()) {
            case WIN:
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((GameActivity) context).win();
                    }
                });
                return true;
            case LOSE:
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((GameActivity) context).lose();
                    }
                });
                return true;
            case CONTINUE:
                return false;
        }
        return false;
    }
}

