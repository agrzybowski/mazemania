package game.modes;

import game.others.Controls;
import game.bot.BotThread;
import game.bot.RandomBotThread;
import game.level.Cell;
import game.level.Player;
import game.level.Point;
import game.others.Trail;

public class RaceMode extends GameMode {
    public RaceMode(Cell[][] map, Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        super(map, end, playerY, playerX, pointsNumber, opponentsNumber);
    }


    @Override
    public void putTrail(Player player, Cell current, Cell next) {
        Trail.basic(player, current, next);
    }

    @Override
    public GameStatus checkGameStatus() {
        for (Point point : points) {
            if (!point.isCollected()) {
                return GameStatus.CONTINUE;
            }
        }
        for (Player opponent : bots) {
            if (opponent.getPointsCollected() > player.getPointsCollected()) {
                return GameStatus.LOSE;
            }
        }
        return GameStatus.WIN;
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis() - gameStartTime;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getWage() {
        return 3;
    }

    @Override
    public int getMode() {
        return raceMode;
    }

    @Override
    public BotThread createBotThread(Controls botControls, Player bot) {
        return new RandomBotThread(botControls, bot);
    }
}
