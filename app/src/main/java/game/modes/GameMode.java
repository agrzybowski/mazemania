package game.modes;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;

import java.util.Random;

import game.others.Controls;
import game.bot.BotThread;
import game.level.Cell;
import game.level.LevelCreator;
import game.level.Player;
import game.level.Point;

public abstract class GameMode {
    public enum GameStatus {
        WIN, LOSE, CONTINUE
    }
    public static final int classicMazeMode = 1;
    public static final int diamondsMode = 2;
    public static final int escapeMode = 3;
    public static final int raceMode = 4;
    public static final int territoryMode = 5;

    static long gameStartTime;
    public static int scorePerLevel = 100000;

    Cell[][] map;
    Point[] points;
    Player[] bots;
    protected Cell end;
    protected Player player;

    int mode = 0;
    int wage = 0;

    GameMode(Cell[][] map, @Nullable Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        this.map = map;
        this.end = end;
        mode = getMode();
        wage = getWage();
        player = createPlayer(playerY, playerX);
        points = createPoints(pointsNumber);
        bots = createOpponents(opponentsNumber);
        gameStartTime = System.currentTimeMillis();
    }

    private Player createPlayer(int y, int x) {
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        return new Player(y, x, paint);
    }

    public Player getPlayer() {
        return player;
    }

    public Cell getEnd() {
        return end;
    }

    public abstract void putTrail(Player player, Cell current, Cell next);

    public abstract GameStatus checkGameStatus();

    public abstract long getTime();

    public abstract void onDestroy();

    public abstract int getWage();

    public abstract int getMode();

    public abstract BotThread createBotThread(Controls botControls, Player bot);

    public Player[] createOpponents(int opponentsNumber) {
        Player[] opponents = new Player[opponentsNumber];
        Random randomGenerator = new Random();
        for (int i = 0; i < opponentsNumber; i++) {
            int opponentY = randomGenerator.nextInt(LevelCreator.levelHeight);
            int opponentX = randomGenerator.nextInt(LevelCreator.levelWidth);

            Paint paint = new Paint();
            paint.setColor(Color.argb(255, randomGenerator.nextInt(256), randomGenerator.nextInt(256), randomGenerator.nextInt(256)));
            opponents[i] = new Player(opponentY, opponentX, paint);
        }
        return opponents;
    }

    public Point[] createPoints(int pointsNumber) {
        Point[] points = new Point[pointsNumber];
        Random randomGenerator = new Random();
        for (int i = 0; i < pointsNumber; i++) {
            int pointY;
            int pointX;
            do {
                pointY = randomGenerator.nextInt(LevelCreator.levelHeight);
                pointX = randomGenerator.nextInt(LevelCreator.levelWidth);
            }
            while (map[pointY][pointX].getClass().equals(Point.class));
            points[i] = new Point(map[pointY][pointX]);
            map[pointY][pointX] = points[i];
        }
        return points;
    }

    public Player[] getBots() {
        return bots;
    }

    public Cell[][] getMap() {
        return map;
    }

    public int getScore() {
        int score = wage * (10 * LevelCreator.levelWidth * LevelCreator.levelHeight + bots.length * 500 + points.length * 200 - (int)(getTime() / 1000));
        if (score > 0) {
            return score;
        } else return 0;
    }
}
