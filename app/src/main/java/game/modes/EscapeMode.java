package game.modes;

import game.others.Controls;
import game.bot.BotThread;
import game.bot.RandomBotThread;
import game.level.Cell;
import game.level.Player;
import game.others.Trail;

public class EscapeMode extends GameMode {
    int[][] directions;

    public EscapeMode(Cell[][] map, Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        super(map, end, playerY, playerX, pointsNumber, opponentsNumber);
        directions = new int[][]{{1, -1, 0, 0}, {0, 0, -1, 1}};
    }

    @Override
    public void putTrail(Player player, Cell current, Cell next) {
        Trail.followPlayer(player, current, next);
    }

    @Override
    public GameStatus checkGameStatus() {
        if (map[player.getY()][player.getX()].equals(end)) {
            return GameStatus.WIN;
        }

        for (Player opponent : bots) {
            if (opponent.getX() == player.getX() && opponent.getY() == player.getY()) {
                return GameStatus.LOSE;
            }
        }
        return GameStatus.CONTINUE;
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis() - gameStartTime;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getWage() {
        return 6;
    }

    @Override
    public int getMode() {
        return escapeMode;
    }

    @Override
    public BotThread createBotThread(Controls botControls, Player bot) {
        return new RandomBotThread(botControls, bot);
    }
}
