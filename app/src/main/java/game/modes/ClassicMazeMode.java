package game.modes;

import game.others.Controls;
import game.bot.BotThread;
import game.level.Cell;
import game.level.Player;
import game.others.Trail;

public class ClassicMazeMode extends GameMode {

    public ClassicMazeMode(Cell[][] map, Cell end, int playerY, int playerX, int pointsNumber, int opponentsNumber) {
        super(map, end, playerY, playerX, pointsNumber, opponentsNumber);
    }

    @Override
    public void putTrail(Player player, Cell current, Cell next) {
        Trail.followPlayer(player, current, next);
    }

    @Override
    public GameStatus checkGameStatus() {
        if(map[player.getY()][player.getX()].equals(end)) {
            return GameStatus.WIN;
        } else {
            return GameStatus.CONTINUE;
        }
    }

    @Override
    public long getTime() {
        return System.currentTimeMillis() - gameStartTime;
    }

    @Override
    public void onDestroy() {

    }


    @Override
    public int getWage() {
        return 1;
    }

    @Override
    public int getMode() {
        return classicMazeMode;
    }

    @Override
    public BotThread createBotThread(Controls botControls, Player bot) {
        return null;
    }
}
